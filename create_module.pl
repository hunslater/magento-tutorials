use strict;
use warnings;

if (@ARGV < 2) {
    print STDERR "Usage: $0 company module\n";
    exit(1);
}

my $company = $ARGV[0];
my $module = $ARGV[1];

system("mkdir -p app/code/local/$company/$module/Block");
system("mkdir -p app/code/local/$company/$module/controllers");
system("mkdir -p app/code/local/$company/$module/etc");
system("mkdir -p app/code/local/$company/$module/Helper");
system("mkdir -p app/code/local/$company/$module/Model");
system("mkdir -p app/code/local/$company/$module/sql");

# <?xml version="1.0"?>
